package jimenez.Percy;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class Principal {

    static BufferedReader in = new BufferedReader(new InputStreamReader(System.in));

    public static void main(String[] args) throws IOException {

        Camisa camisa1 = new Camisa(Talla.GRANDE, 5000);
        System.out.println("camisa 1: " + camisa1.ToString());
        System.out.println("**********************************************\n");
        
        System.out.println("****BIENVENIDO AL SISTEMA******");
        System.out.println("Por favor ingrese la talla: PEQUEÑO, MEDIANO, GRANDE: ");
        String talla = in.readLine();
        System.out.println("por favor ingrese el precio de la camisa: ");
        double precio = Double.parseDouble(in.readLine());
        
        Talla tallaReal = Talla.valueOf(talla);
        Camisa camisa2 = new Camisa (tallaReal,precio);
        System.out.println("Camisa 2: "+ camisa2.toString());
        
    }

}